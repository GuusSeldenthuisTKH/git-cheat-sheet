# README #

Within this readme you'll will find a handy collection of commonly used GIT commands.

## Basics ##

### Download repository ###

Fetching and pulling are terms used when talking about downloading code from the certain repository.

#### Initializing a folder for future git usage ####

`git init .` (Use `.` when chosen location is the current working directory.)

#### Cloning a repository ####

`git clone #.git-url#` Use -b as extra parameter in order to checkout a specific branch.